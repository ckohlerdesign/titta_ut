import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:se_upp/forecast_request.dart';
import 'package:se_upp/forecast_response.dart';
import 'content/temperature_page_style.dart';

class TemperaturePage extends StatefulWidget {
  const TemperaturePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _TemperaturePageState();
}

class _TemperaturePageState extends State<TemperaturePage> {
  Future<ForecastResponse> getData(bool isCurrentCity, String cityName) async {
    return await CallToApi().callWeatherApi(isCurrentCity, cityName);
  }

  TextEditingController textController = TextEditingController(text: "");
  Future<ForecastResponse>? _myData;

  @override
  void initState() {
    setState(() {
      _myData = getData(true, "");
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: FutureBuilder(
        builder: (ctx, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            // If error occured
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error.toString()} occurred',
                  style: const TextStyle(fontSize: 18),
                ),
              );
            } else if (snapshot.hasData) {
              final data = snapshot.data as ForecastResponse;

              final sunriseTime =
                  DateTime.fromMillisecondsSinceEpoch(data.sunrise * 1000);
              final sunsetTime =
                  DateTime.fromMillisecondsSinceEpoch(data.sunset * 1000);
              final currentTimeStamp = DateTime.now();
              String weatherCondition = data.description.toLowerCase();

              bool isDay = false;
              if (currentTimeStamp.isAfter(sunriseTime) &&
                  currentTimeStamp.isBefore(sunsetTime)) {
                isDay = true;
              }

              String getBackgroundImagePath() {
                String condition = weatherCondition.toLowerCase();
                if (isDay) {
                  switch (condition) {
                    case 'klart':
                      return 'lib/assets/images/Sol.png';
                    case 'molnigt':
                      return 'lib/assets/images/Moln.png';
                    case 'växlande molnighet':
                      return 'lib/assets/images/Moln.png';
                    case 'mulet':
                      return 'lib/assets/images/Moln.png';
                    case 'regn':
                      return 'lib/assets/images/Regn.png';
                    case 'lätt regn':
                      return 'lib/assets/images/Regn.png';
                    case 'åska':
                      return 'lib/assets/images/Åska.png';
                    case 'snö':
                      return 'lib/assets/images/Snö.png';
                    case 'dimma':
                      return 'lib/assets/images/Moln.png';
                    case 'dis':
                      return 'lib/assets/images/Moln.png';
                    case 'rök':
                      return 'lib/assets/images/Moln.png';
                    case 'damm':
                      return 'lib/assets/images/Moln.png';
                    case 'sand':
                      return 'lib/assets/images/Moln.png';
                    case 'squalls':
                      return 'lib/assets/images/Moln.png';
                    case 'tornado':
                      return 'lib/assets/images/Moln.png';
                    case 'duggregn':
                      return 'lib/assets/images/Regn.png';
                    case 'skurar':
                      return 'lib/assets/images/Regn.png';
                    case 'regn och snö':
                      return 'lib/assets/images/Snö.png';
                    case 'lätt åska':
                      return 'lib/assets/images/Åska.png';
                    case 'kraftig åska':
                      return 'lib/assets/images/Åska.png';
                    case 'lätt duggregn':
                      return 'lib/assets/images/Regn.png';
                    case 'kraftigt duggregn':
                      return 'lib/assets/images/Regn.png';
                    case 'lätt åska med regn':
                      return 'lib/assets/images/Åska.png';
                    case 'kraftig åska med regn':
                      return 'lib/assets/images/Åska.png';
                    case 'lätt åska med snö':
                      return 'lib/assets/images/Åska.png';
                    case 'kraftig åska med snö':
                      return 'lib/assets/images/Åska.png';
                    case 'lätt åska med duggregn':
                      return 'lib/assets/images/Åska.png';
                    case 'kraftig åska med duggregn':
                      return 'lib/assets/images/Åska.png';
                    default:
                      return 'lib/assets/images/Sol.png';
                  }
                } else {
                  switch (condition) {
                    case 'klart':
                      return 'lib/assets/images/Natt_klar.png';
                    case 'molnigt':
                      return 'lib/assets/images/Natt_moln.png';
                    case 'mulet':
                      return 'lib/assets/images/Natt_moln.png';
                    case 'regn':
                      return 'lib/assets/images/Natt_regn.png';
                    case 'lätt regn':
                      return 'lib/assets/images/Natt_regn.png';
                    case 'åska':
                      return 'lib/assets/images/Natt_åska.png';
                    case 'snö':
                      return 'lib/assets/images/Natt_snö.png';
                    case 'dimma':
                      return 'lib/assets/images/Natt_moln.png';
                    case 'dis':
                      return 'lib/assets/images/Natt_moln.png';
                    case 'rök':
                      return 'lib/assets/images/Natt_moln.png';
                    case 'damm':
                      return 'lib/assets/images/Natt_moln.png';
                    case 'sand':
                      return 'lib/assets/images/Natt_moln.png';
                    case 'squalls':
                      return 'lib/assets/images/Natt_moln.png';
                    case 'tornado':
                      return 'lib/assets/images/Natt_moln.png';
                    case 'duggregn':
                      return 'lib/assets/images/Natt_regn.png';
                    case 'skurar':
                      return 'lib/assets/images/Natt_regn.png';
                    case 'regn och snö':
                      return 'lib/assets/images/Natt_snö.png';
                    case 'lätt åska':
                      return 'lib/assets/images/Natt_åska.png';
                    case 'kraftig åska':
                      return 'lib/assets/images/Natt_åska.png';
                    case 'lätt duggregn':
                      return 'lib/assets/images/Natt_regn.png';
                    case 'kraftigt duggregn':
                      return 'lib/assets/images/Natt_regn.png';
                    case 'lätt åska med regn':
                      return 'lib/assets/images/Natt_regn.png';
                    case 'kraftig åska med regn':
                      return 'lib/assets/images/Natt_åska.png';
                    case 'lätt åska med snö':
                      return 'lib/assets/images/Natt_åska.png';
                    case 'kraftig åska med snö':
                      return 'lib/assets/images/Natt_åska.png';
                    case 'lätt åska med duggregn':
                      return 'lib/assets/images/Natt_åska.png';
                    case 'kraftig åska med duggregn':
                      return 'lib/assets/images/Natt_åska.png';
                    default:
                      return 'lib/assets/images/Natt_klar.png';
                  }
                }
              }

              void handleRefresh() {
                CallToApi().callWeatherApi(true, data.city).then((_) {
                  setState(() {});
                });
              }

              String backgroundImagePath = getBackgroundImagePath();

              return Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                decoration: BoxDecoration(
                    image: DecorationImage(
                  image: AssetImage(backgroundImagePath),
                  fit: BoxFit.cover,
                )),
                width: double.infinity,
                height: double.infinity,
                child: SafeArea(
                  child: Column(
                    children: [
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            AutoSizeText(
                              data.city.toUpperCase(),
                              style: city,
                              textAlign: TextAlign.center,
                              maxLines: 1,
                            ),
                            AutoSizeText(
                              data.description.capitalize(),
                              style: description,
                              textAlign: TextAlign.center,
                              maxLines: 1,
                            ),
                            AutoSizeText(
                              "${data.temp.round()}°C",
                              style: temp,
                              textAlign: TextAlign.center,
                              maxLines: 1,
                            ),
                            if (data.temp.round() != data.feelslike.round())
                              (AutoSizeText(
                                  "Känns som: ${data.feelslike.round()}°C",
                                  style: feelslike,
                                  maxLines: 1,
                                  textAlign: TextAlign.center)),
                            FloatingActionButton(
                              backgroundColor:
                                  const Color.fromARGB(255, 5, 245, 221),
                              foregroundColor: Colors.black,
                              onPressed: () {
                                handleRefresh();
                              },
                              child: const Icon(Icons.refresh),
                            ),
                            const Padding(padding: EdgeInsets.only(bottom: 150))
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }
          } else if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return Center(
              child: Text("${snapshot.connectionState} occured"),
            );
          }
          return const Center(
            child: Text("Server timed out!"),
          );
        },
        future: _myData!,
      ),
    );
  }
}
