import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:se_upp/forecast_response.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'api_key.dart';

class CallToApi {
  Future<ForecastResponse> callWeatherApi(bool current, String cityName) async {
    try {
      Position currentPosition = await getCurrentPosition();
      if (current) {
        List<Placemark> placemarks = await placemarkFromCoordinates(
            currentPosition.latitude, currentPosition.longitude);
        Placemark place = placemarks[0];
        cityName = place.locality!;
      }

      var url = Uri.https('api.openweathermap.org', '/data/2.5/weather',
          {'q': cityName, "units": "metric", "lang": "se", "appid": apiKey});

      final http.Response response = await http.get(url);

      if (response.statusCode == 200) {
        var jsonString = response.body;
        Map<String, dynamic> decodedJson = json.decode(jsonString);
        return ForecastResponse.fromMap(decodedJson);
      } else {
        throw Exception('Fel 1:');
      }
    } catch (e) {
      throw Exception('Fel 2: $e');
    }
  }

  Future<Position> getCurrentPosition() async {
    bool serviceEnabled;
    LocationPermission permission;
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Dela din plats är avaktiviterat');
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
    return await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.best,
    );
  }
}
