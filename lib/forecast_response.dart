
class ForecastResponse {
  final double temp;
  final String city;
  final String description;
  final double feelslike;
  final int id;
  final int sunrise;
  final int sunset;

  ForecastResponse.fromMap(Map<String, dynamic> json)
      : temp = json['main']['temp'],
        city = json['name'],
        description = json['weather'][0]['description'],
        feelslike = json['main']['feels_like'],
        id = json['weather'][0]['id'],
        sunrise = json['sys']['sunrise'],
        sunset = json['sys']['sunset'];
}
