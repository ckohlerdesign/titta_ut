import 'package:flutter/material.dart';

//City Text
TextStyle city = const TextStyle(
  fontFamily: 'SF-Pro-Rounded-Bold',
  fontSize: 54,
  color: Colors.white,
  fontWeight: FontWeight.w700,
);

//Description
TextStyle description = const TextStyle(
  fontFamily: 'SF-Pro-Rounded-Bold',
  fontSize: 34,
  color: Colors.white,
  fontWeight: FontWeight.w700,
);
//Temperature
TextStyle temp = const TextStyle(
  fontFamily: 'SF-Pro-Rounded-Bold',
  fontSize: 90,
  color: Colors.white,
  fontWeight: FontWeight.w700,
);
TextStyle feelslike = const TextStyle(
  fontFamily: 'SF-Pro-Rounded-Bold',
  fontSize: 34,
  color: Colors.white,
  fontWeight: FontWeight.w700,
);
TextStyle fluff = const TextStyle(
  fontFamily: 'SF-Pro-Rounded-Bold',
  fontSize: 18,
  color: Colors.white,
  fontWeight: FontWeight.w700,
);

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${substring(1).toLowerCase()}";
  }
}
